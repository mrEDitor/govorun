package io.github.mreditor.govorun.extensions

import io.github.mreditor.govorun.corpora.Corpora
import io.github.mreditor.govorun.corpora.FormComparator
import java.util.regex.Pattern
import java.util.regex.Pattern.*
import kotlin.math.max
import kotlin.math.min

private val useUnicode = CASE_INSENSITIVE or UNICODE_CASE or UNICODE_CHARACTER_CLASS
private val range = Pattern.compile("(\\d+)-(\\d+)")
private val term = Pattern.compile("[?!.]|[\\w-]+(?:[.'@][\\w-]+)*", useUnicode).toRegex()
private val corpora = Corpora()

/**
 * Get range from string
 */
fun String.toRange(): IntRange {
    val matcher = range.matcher(this)
    if (matcher.matches() && matcher.groupCount() == 2) {
        val from = matcher.group(1).toInt()
        val to = matcher.group(2).toInt()
        return from..to
    }
    throw IllegalArgumentException("Unable to parse range ${this}")
}

/**
 * Get alphanumeric lowercase terms from string
 */
fun String.toTerms() = term.findAll(this).map { it.value }

/**
 * Get comparable forms list from string
 */
fun String.toComparableForms() = FormComparator.map(term.findAll(this).map {
    it.value.toLowerCase()
}.toList())

/**
 * Damerau–Levenshtein distance: optimal string alignment distance
 * Wagner–Fischer algorithm
 */
fun String.distance(another: String): Float {
    val len1 = this.length
    val len2 = another.length
    val arr = Array(len1 + 1) { IntArray(len2 + 1) }
    for (i in 0..len1) {
        arr[i][0] = i
    }
    for (i in 1..len2) {
        arr[0][i] = i
    }
    for (i in 1..len1) for (j in 1..len2) {
        val m = if (this[i - 1] == another[j - 1]) 0 else 1
        arr[i][j] = min(min(arr[i - 1][j] + 1, arr[i][j - 1] + 1), arr[i - 1][j - 1] + m)
    }
    return arr[len1][len2] / max(len1, len2).toFloat()

}