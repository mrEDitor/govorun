package io.github.mreditor.govorun.extensions

import com.google.gson.JsonElement
import com.google.gson.JsonParseException

/**
 * Fetch data by path from json element
 */
fun JsonElement.path(path: String) = path(path.split("."))

/**
 * Fetch data by path from json element
 */
fun JsonElement.path(path: List<String>) = path(path.iterator())

/**
 * Fetch data by path from json element
 */
fun JsonElement.path(path: Iterator<String>): JsonElement {
    try {
        if (!path.hasNext()) {
            return this
        } else if (isJsonObject) {
            return asJsonObject[path.next()].path(path)
        } else if (isJsonArray) {
            return asJsonArray[path.next().toInt()].path(path)
        }
    } catch (e: Exception) {
        throw JsonParseException(e)
    }
    throw JsonParseException("Specified JSON path does not exists!")
}
