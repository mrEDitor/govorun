package io.github.mreditor.govorun.extensions

import io.github.mreditor.govorun.corpora.FormComparator

val stops = arrayOf(".", "?", "!")

/**
 * Get term position in sentence
 */
fun List<FormComparator>.positionInSentence(offset: Int): Float {
    val previousStop = lastIndexOf(*stops, toIndex = offset)
    val nextStopOrNone = indexOf(*stops, fromIndex = offset)
    val nextStop = if (nextStopOrNone < 0) size else nextStopOrNone
    return (offset - previousStop) / (nextStop - previousStop - 1).toFloat()
}
