package io.github.mreditor.govorun.extensions

import java.util.*
import kotlin.math.max
import kotlin.math.min

/**
 * Find first matching sublist in list.
 * Returns -1 if no matches found.
 */
fun <E> List<E>.indexOfSubList(needle: List<E>) = Collections.indexOfSubList(this, needle)

/**
 * Get index of last occurrence of [needle] in [this] since [fromIndex] (inclusively).
 * Returned value is ret <= toIndex && this[ret] == needle.
 * If no such value exists, -1 is returned.
 */
fun <E> List<E>.indexOf(vararg needle: E, fromIndex: Int = 0): Int {
    for (i in max(0, fromIndex) until this.size) {
        if (this[i] in needle) {
            return i
        }
    }
    return -1
}

/**
 * Get index of last occurrence of [needle] in [this] until [toIndex] (inclusively).
 * Returned value is ret <= toIndex && this[ret] == needle.
 * If no such value exists, -1 is returned.
 */
fun <E> List<E>.lastIndexOf(vararg needle: E, toIndex: Int = size - 1): Int {
    for (i in min(toIndex, this.size - 1) downTo 0) {
        if (this[i] in needle) {
            return i
        }
    }
    return -1
}

/**
 * Count matches of [sublist] in [this].
 */
fun <E : Any> List<E>.countOf(sublist: List<E>): Int {
    var count = 0
    for (i in 0 until this.size) {
        for (j in 0 until sublist.size) {
            if (i + j >= this.size || this[i + j] != sublist[j]) {
                --count
                break
            }
        }
        ++count
    }
    return count
}

/**
 * Count matches of [needle] in [this].
 */
fun <E : Any> List<E>.countOf(needle: E): Int = count(needle::equals)
