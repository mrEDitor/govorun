package io.github.mreditor.govorun

import java.io.File
import java.util.*

/**
 * CLI application helper class
 *
 * @param args are trimmed out of left hyphens, so `--option`, `-option` and
 * `option` are equivalent
 */
class CLI private constructor(args: Array<String>) {

    /**
     * User input exception
     */
    class ArgumentException(val key: String, cause: Throwable? = null)
        : kotlin.Exception("Command-line argument ${key} missed or invalid", cause)

    /**
     * Configuration exception
     */
    class ConfigurationException(val key: String, cause: Throwable? = null)
        : kotlin.Exception("Configuration key '${key}' missed or invalid", cause) {
        /**
         * Nor user input, nor configuration acceptable
         */
        constructor(key: String, configKey: String) : this(key, ArgumentException(configKey))
    }

    /**
     * Initialization logic
     */
    companion object {
        private var context = Stack<CLI>()

        fun active() = context.peek()

        fun launch(args: Array<String>, scenario: CLI.() -> Unit) {
            val cli = CLI(args)
            context.push(cli)
            cli.apply(scenario)
            context.pop()
        }
    }

    /**
     * Exploded by '=' key-value argument list
     */
    private val arguments = args.associate {
        val parts = it.trimStart { it == '-' }.split("=", limit = 2)
        return@associate Pair(parts[0], parts.getOrNull(1))
    }

    /**
     * Settings
     */
    private val settings = Properties().apply {
        val configuration = File(arguments["use-config"] ?: "govorun.cfg")
        load(configuration.reader())
    }.toList().associate { it.first.toString() to it.second.toString() }

    /**
     * Registered commands with their descriptions
     */
    private val commands = mutableListOf<List<String>>()

    /**
     * Whether any command throw exception
     */
    var anyCommandFailed = false
        private set

    /**
     * Whether any command success
     */
    var anyCommandSuccess = false
        private set

    /**
     * Get [argument] or [setting] if no argument found,
     * null key skips search among arguments or settings accordingly.
     * Both keys should not be null.
     */
    fun getString(argument: String? = null, setting: String? = null): String {
        val s = getStringOrNull(argument, setting)
        if (s != null) {
            return s
        }

        if (setting != null && argument != null) {
            throw ConfigurationException(setting, argument)
        } else if (setting != null) {
            throw ConfigurationException(setting)
        } else if (argument != null) {
            throw ArgumentException(argument)
        }
        throw Exception("Fetching option from nowhere")
    }

    /**
     * Nullable [getString]
     */
    fun getStringOrNull(argument: String? = null, setting: String? = null): String? {
        if (argument != null && arguments[argument] != null) {
            return arguments[argument]
        }
        if (setting != null && settings[setting] != null) {
            return settings[setting]
        }
        return null
    }

    /**
     * Get current arguments + settings
     */
    fun dumpConfiguration() =
            (arguments.map { "--${it.key}=${it.value}" } + settings.map { "${it.key}=${it.value}" })
                    .sorted()

    /**
     * Executing [action] if [key] is active argument
     */
    fun registerCommand(key: String, value: String?, help: String, action: CLI.() -> Unit): CLI {
        commands.add(listOf(key, value ?: "", help))
        if (key in arguments) try {
            this.action()
            anyCommandSuccess = true
        } catch (e: Exception) {
            anyCommandFailed = true
            System.out.println()
            e.printStackTrace()
        }
        return this
    }

    /**
     * Show help
     */
    fun showHelp(): CLI {
        System.out.println("Available options:")
        for ((key, value, help) in commands) {
            if (value.isEmpty()) {
                System.out.println("\t--${key}")
            } else {
                System.out.println("\t--${key}=${value}")
            }
            System.out.println("\t\t${help}")
        }
        return this
    }

}
