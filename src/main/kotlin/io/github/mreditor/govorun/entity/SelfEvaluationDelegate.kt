package io.github.mreditor.govorun.entity

import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

/**
 * Self-evaluation delegate.
 * @see [Entity.lock] for updating all such delegates in entity at once
 */
open class SelfEvaluationDelegate<Type>(
        protected val expr: Entity.() -> Type
) : ReadOnlyProperty<Entity, Type> {

    /**
     * Delegated value
     */
    protected var value: Type? = null

    /**
     * Read value from provided expression
     */
    override operator fun getValue(thisRef: Entity, property: KProperty<*>): Type {
        if (!thisRef.locked) {
            value = thisRef.expr()
        }
        return value as Type
    }

}
