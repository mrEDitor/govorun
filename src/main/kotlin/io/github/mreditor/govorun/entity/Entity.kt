package io.github.mreditor.govorun.entity

import io.github.mreditor.govorun.LRUCache
import org.jetbrains.exposed.dao.*
import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.Op
import org.jetbrains.exposed.sql.SizedIterable
import org.jetbrains.exposed.sql.SqlExpressionBuilder
import java.math.BigDecimal
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty
import kotlin.reflect.KProperty1
import kotlin.reflect.full.memberProperties
import kotlin.reflect.jvm.isAccessible

/**
 * Abstract entity class.
 * Implements lock ability, which guarantee that var field delegated by
 * [LockableColumnWrapper] will not be changed.
 */
abstract class Entity(id: EntityID<Int>) : IntEntity(id) {

    companion object {
        const val DECIMAL_PRECISION_DEFAULT = 8
        const val DECIMAL_SCALE_DEFAULT = 7
    }

    /**
     * Table definition class
     */
    abstract class Table(name: String) : IntIdTable(name) {
        /**
         * Get pre-defined decimal column type
         */
        protected fun decimal(name: String) =
                decimal(name, DECIMAL_PRECISION_DEFAULT, DECIMAL_SCALE_DEFAULT)
    }

    /**
     * Entity factory class
     */
    abstract class Factory<T : Entity>(table: Table) : EntityClass<Int, T>(table) {

        /**
         * Cache
         */
        protected val cache = LRUCache<SqlExpressionBuilder.() -> Op<Boolean>, List<T>>()

        /**
         * Retrieve cached
         */
        fun findInCache(query: SqlExpressionBuilder.() -> Op<Boolean>) =
                cache.getOrPut(query) {
                    find(query).toList().apply {
                        forEach { it.lock(false) }
                    }
                }

        /**
         * Get nth element from collection
         */
        fun getNth(collection: SizedIterable<T>, n: Int) =
                collection.limit(1, n).first().apply { lock() }

    }

    /**
     * Self-evaluation delegate.
     * @see [Entity.lock] for updating all such delegates in entity at once
     */
    inner class SelfEvaluatingColumnWrapper<Type>(
            expr: Entity.() -> Type,
            private val column: Column<Type>
    ) : SelfEvaluationDelegate<Type>(expr) {

        /**
         * Is column locked
         */
        private var locked = false

        /**
         * Read value from provided expression
         */
        override operator fun getValue(thisRef: Entity, property: KProperty<*>): Type {
            if (!thisRef.locked && !this.locked) {
                value = thisRef.expr()
                column.setValue(thisRef, property, value as Type)
            } else {
                value = column.getValue(thisRef, property)
            }
            if (thisRef.locking) {
                this.locked = true
            }
            return value as Type
        }

    }

    /**
     * Lockable field delegate.
     * @see [Entity.lock] for locking all such delegates in entity at once
     */
    inner class LockableColumnWrapper<Type>(
            private val column: Column<Type>
    ) : ReadWriteProperty<Entity, Type> {

        /**
         * Read value from provided column
         */
        override operator fun getValue(thisRef: Entity, property: KProperty<*>) =
                column.getValue(thisRef, property)

        /**
         * Write value to provided column, if object is not locked
         */
        override operator fun setValue(thisRef: Entity, property: KProperty<*>, value: Type) {
            if (thisRef.locked) {
                throw IllegalAccessException("Object is locked")
            }
            column.setValue(thisRef, property, value)
        }

    }

    /**
     * Lockable field delegate.
     * @see [Entity.lock] for locking all such delegates in entity at once
     */
    inner class LockableEntityReferenceWrapper<Type : IntEntity>(
            private val reference: Reference<Int, Type>
    ) : ReadWriteProperty<Entity, Type> {

        /**
         * Read value from provided expression
         */
        override operator fun getValue(thisRef: Entity, property: KProperty<*>) =
                reference.getValue(thisRef, property)

        /**
         * Read value from provided expression
         */
        override operator fun setValue(thisRef: Entity, property: KProperty<*>, value: Type) {
            if (thisRef.locked) {
                throw IllegalAccessException("Object is locked")
            }
            reference.setValue(thisRef, property, value)
        }

    }

    /**
     * Self-evaluation delegate as operation alias
     */
    protected fun <T> evaluation(expr: Entity.() -> T) =
            SelfEvaluationDelegate(expr)

    /**
     * Self-evaluation delegate for columns as operation alias
     */
    protected infix fun <T> Column<T>.evaluates(expr: Entity.() -> T) =
            SelfEvaluatingColumnWrapper(expr, this)

    /**
     * Lockable field delegate for column as operation alias
     */
    protected fun <T> lockable(column: Column<T>) = LockableColumnWrapper(column)

    /**
     * Lockable reference field delegate as operations alias
     */
    protected fun <T : IntEntity> lockable(ref: Reference<Int, T>) = LockableEntityReferenceWrapper(ref)

    /**
     * Update all columns delegated by SelfEvaluatingColumnWrapper and lock object
     */
    open fun lock(update: Boolean = true) {
        if (locked) {
            return
        }
        locking = true
        if (update) for (property in this::class.memberProperties) {
            val kProperty = property as? KProperty1<Entity, *>
                    ?: throw ReflectiveOperationException()
            kProperty.isAccessible = true
            val delegate = kProperty.getDelegate(this)
            if (delegate is SelfEvaluationDelegate<*>) {
                delegate.getValue(this, property)
            }
        }
        locked = true
    }

    /**
     * Short decimal cast
     */
    protected fun decimal(v: Double) = BigDecimal.valueOf(v)

    /**
     * Short decimal cast
     */
    protected fun decimal(v: Int) = decimal(v.toLong())

    /**
     * Short decimal cast
     */
    protected fun decimal(v: Long) = BigDecimal.valueOf(v, DECIMAL_SCALE_DEFAULT)

    /**
     * Short decimal cast
     */
    protected fun decimal(v: Int?) = if (v == null) null else decimal(v)

    /**
     * Short decimal cast
     */
    protected fun decimal(v: Long?) = if (v == null) null else decimal(v)

    /**
     * Is property locked
     */
    var locked = false
        protected set

    /**
     * Is property locking now
     */
    protected var locking = false

}
