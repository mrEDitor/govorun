package io.github.mreditor.govorun

import io.github.mreditor.govorun.corpora.Form
import io.github.mreditor.govorun.corpora.Lemma
import io.github.mreditor.govorun.corpora.LemmaGrammeme
import io.github.mreditor.govorun.corpora.Wiki
import io.github.mreditor.govorun.document.Document
import io.github.mreditor.govorun.document.DocumentTag
import io.github.mreditor.govorun.document.Vocabulary
import io.github.mreditor.govorun.entity.Entity
import io.github.mreditor.govorun.perceptron.Configuration
import io.github.mreditor.govorun.perceptron.Perceptron
import io.github.mreditor.govorun.CLI.ConfigurationException as ConfigException
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.Transaction
import org.jetbrains.exposed.sql.transactions.transaction
import org.jetbrains.exposed.sql.Database as ExposedDatabase

/**
 * Database connection manager
 */
class Database {
    companion object {

        /**
         * Count of operations per
         */
        val batchSize = CLI.active().getString("db.batch-size", "db.batch-size").toInt()

        lateinit var connection: ExposedDatabase

        /**
         * Connect to database and ensure its table structure
         */
        fun connect() {
            connection = ExposedDatabase.connect(
                    CLI.active().getString("db.url", "db.url"),
                    CLI.active().getString("db.driver", "db.driver"),
                    CLI.active().getString("db.username", "db.username"),
                    CLI.active().getString("db.password", "db.password")
            )
            createTables()
        }

        /**
         * Create missing tables
         */
        fun createTables() {
            transaction {
                SchemaUtils.createMissingTablesAndColumns(
                        Lemma.Table,
                        Form.Table,
                        LemmaGrammeme.Table,
                        Document.Table,
                        DocumentTag.Table,
                        Vocabulary.Table,
                        Wiki.Table,
                        Configuration.Table
                )
            }
        }

        /**
         * Continuously aggregate [aBody] block to transactions by
         * [aIterationsPerCommit] until [aWhile] is false
         */
        fun aggregateToTransactions(
                aWhile: () -> Boolean,
                aIterationsPerCommit: Int = batchSize,
                aBody: Transaction.() -> Unit
        ) {
            while (aWhile()) {
                transaction {
                    var iteration = 0
                    while (aWhile() && ++iteration < aIterationsPerCommit) {
                        aBody()
                    }
                }
            }
        }

        fun dropTables(vararg tables: Entity.Table) {
            transaction {
                SchemaUtils.drop(*tables)
            }
        }
    }

}
