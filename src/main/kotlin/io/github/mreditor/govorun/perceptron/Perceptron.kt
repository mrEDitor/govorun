package io.github.mreditor.govorun.perceptron

import io.github.mreditor.govorun.document.DocumentPhrase

class Perceptron(vararg val layers: PerceptronLayer) {

    /**
     * Error
     */
    var error = 0.0

    /**
     * Adapt perceptron to given sample
     */
    fun adapt(sample: DocumentPhrase, speed: Float) {
        propagateForward(sample)
        propagateBackward(sample, speed)
    }

    /**
     * Solve sample set
     */
    fun solve(sample: DocumentPhrase) {
        propagateForward(sample)
    }

    /**
     * Forward feed
     */
    fun propagateForward(sample: DocumentPhrase) {
        layers[0].advance(sample)
        for (i in 1 until layers.size) {
            layers[i].advance(layers[i - 1])
        }
    }

    /**
     * Back propagation
     */
    fun propagateBackward(sample: DocumentPhrase, speed: Float) {
        error += output.outputError(sample.output)
        for (i in layers.size - 2 downTo 0) {
            layers[i].calculateError(layers[i + 1])
        }
        layers[0].calculateWeight(sample, speed)
        for (i in 1 until layers.size) {
            layers[i].calculateWeight(layers[i - 1], speed)
        }
    }

    /**
     * Clear error
     */
    fun clearError() {
        error = 0.0
    }

    /**
     * Output layer
     */
    val output
        get() = layers[layers.size - 1]

}