package io.github.mreditor.govorun.perceptron

import java.util.*

class PerceptronLayer(outputSize: Int, val random: Random) : SourceLayer {
    val activation = SigmoidFunction()

    /**
     * In format: weight.get(source)[output]
     */
    protected val weight = mutableListOf<FloatArray>()

    val error = FloatArray(outputSize) { 0.0f }

    val input = FloatArray(outputSize) { 0.0f }

    val output = FloatArray(outputSize) { 0.0f }

    override val external: FloatArray
        get() = input

    fun advance(sourceLayer: SourceLayer) {
        val source = sourceLayer.external
        val weights = getWeights(source.size)
        for (i in output.indices) {
            val out = source.indices.sumByDouble {
                (weights[it][i] * source[it]).toDouble()
            }.toFloat() + weights[source.size][i]
            input[i] = out
            output[i] = activation.function(out)
        }
    }

    fun getWeights(size: Int): List<FloatArray> {
        if (weight.isEmpty()) {
            for (i in 0..size) {
                val weights = FloatArray(output.size) { random.nextFloat() - 0.5f }
                weight.add(weights)
            }
        } else if (size + 1 != weight.size) {
            throw IllegalArgumentException()
        }
        return weight
    }

    fun outputError(answer: FloatArray): Float {
        if (answer.size != output.size) {
            throw IllegalArgumentException()
        }

        var outputError = 0.0
        for (i in answer.indices) {
            error[i] = answer[i] - output[i]
            outputError += error[i] * error[i]
        }
        return outputError.toFloat()
    }

    fun calculateError(source: PerceptronLayer) {
        val errors = source.error
        val weights = source.getWeights(output.size)
        for (i in output.indices) {
            var sum = 0.0f
            val neuronWeights = weights[i]
            if (neuronWeights.size != errors.size) {
                throw IllegalArgumentException()
            }
            for (j in neuronWeights.indices) {
                sum += errors[j] * neuronWeights[j]
            }
            error[i] = activation.differential(output[i]) * sum
        }
    }

    fun calculateWeight(layer: SourceLayer, speed: Float) {
        val source = layer.external
        val weight = getWeights(source.size)
        for (i in output.indices) {
            val e = error[i] * activation.differential(input[i])
            for (j in source.indices) {
                weight[j][i] += speed * e * source[j]
            }
            weight[source.size][i] += speed * e
        }
    }

}
