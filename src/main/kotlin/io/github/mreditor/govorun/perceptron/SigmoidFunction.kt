package io.github.mreditor.govorun.perceptron

import kotlin.math.exp

class SigmoidFunction {

    /**
     * Function
     */
    fun function(value: Float) = 1.0f / (1.0f + exp(-value))

    /**
     * Differential
     */
    fun differential(value: Float) = function(value) * (1.0f - function(value))

}
