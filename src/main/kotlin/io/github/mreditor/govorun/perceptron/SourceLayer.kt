package io.github.mreditor.govorun.perceptron

interface SourceLayer {

    /**
     * Output data
     */
    val external: FloatArray

}
