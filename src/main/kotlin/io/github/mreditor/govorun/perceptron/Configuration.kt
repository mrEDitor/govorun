package io.github.mreditor.govorun.perceptron

import com.google.gson.Gson
import io.github.mreditor.govorun.entity.Entity
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.sql.transactions.transaction
import java.math.BigDecimal

/**
 * Perceptron Configuration
 */
class Configuration(id: EntityID<Int>) : Entity(id) {

    object Factory : Entity.Factory<Configuration>(Table) {

        /**
         * Get perceptron or save given
         */
        fun getOrSave(perceptron: Perceptron): Configuration {
            return transaction {
                if (all().count() > 0) {
                    all().first()
                } else {
                    new {
                        save(perceptron)
                        error = BigDecimal.ONE
                    }
                }
            }
        }

        /**
         * Get saved perceptron
         */
        fun get() = transaction { all().first() }

    }

    object Table : Entity.Table("perceptron") {
        val configuration = text("configuration")
        val error = decimal("error")
    }

    /**
     * Configuration
     */
    var configuration by Table.configuration

    /**
     * Error
     */
    var error by Table.error

    /**
     * Json utils
     */
    val gson = Gson()

    /**
     * Load perceptron
     */
    fun load(): Perceptron =
            gson.fromJson(configuration, Perceptron::class.java)

    /**
     * Save perceptron
     */
    fun save(perceptron: Perceptron) {
        transaction {
            configuration = gson.toJson(perceptron)
            error = decimal(perceptron.error)
            System.out.println("${error} minimal error achieved")
        }
    }

}
