package io.github.mreditor.govorun.corpora

import com.google.gson.JsonElement
import com.google.gson.JsonParseException
import com.google.gson.JsonParser
import io.github.mreditor.govorun.CLI
import io.github.mreditor.govorun.entity.Entity
import io.github.mreditor.govorun.extensions.distance
import io.github.mreditor.govorun.extensions.path
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.sql.Op
import org.jetbrains.exposed.sql.SqlExpressionBuilder
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.joda.time.DateTime
import java.math.BigDecimal
import java.net.URL

/**
 * Wiki-based co-corpora, providing weak-reliable named entities dictionary
 */
class Wiki(id: EntityID<Int>) : Entity(id) {

    companion object {
        private val PAGE_URL_TEMPLATE = CLI.active().getString(setting = "search.url.search")
        private val PAGE_TITLE_PATH = CLI.active().getString(setting = "search.path.title")
        private val CACHE_DAYS = CLI.active().getString(setting = "search.cache.days").toInt()
        private val jsonParser = JsonParser()
    }

    object Factory : Entity.Factory<Wiki>(Table) {

        /**
         * Fetch wiki page by term
         */
        fun create(query: String) = new {
            this.query = query
        }.apply { lock() }

        /**
         * Find wiki page by term or fetch new
         */
        fun find(query: String): Wiki {
            val filter: SqlExpressionBuilder.() -> Op<Boolean> = {
                Table.query eq query
            }
            val wiki = findInCache(filter)
            if (wiki.any()) {
                return wiki.first()
            } else {
                val r = create(query)
                cache.put(filter, listOf(r))
                return r
            }
        }

    }

    object Table : Entity.Table("wiki") {
        val query = varchar("query", 255).uniqueIndex()
        val term = varchar("term", 255).nullable()
        val distance = decimal("distance")
        val updatedAt = date("updated_at").nullable()
    }

    var query by lockable(Table.query)
    var updatedAt by lockable(Table.updatedAt)
    val term by Table.term evaluates {
        if (updateNeeded) try {
            queryResponse.path(PAGE_TITLE_PATH).asJsonPrimitive.asString.toLowerCase()
        } catch (e: JsonParseException) {
            null
        } else {
            Table.term.getValue(this, ::term)
        }
    }
    val distance by Table.distance evaluates {
        term?.distance(query)?.toBigDecimal() ?: BigDecimal.ONE
    }

    /**
     * Is update needed
     */
    private val updateNeeded
        get() = updatedAt?.plusDays(Wiki.CACHE_DAYS)?.isBeforeNow ?: true

    /**
     * Wiki query response
     */
    val queryResponse by evaluation {
        while (true) try {
            val reader = URL(PAGE_URL_TEMPLATE.format(query)).openStream().reader()
            return@evaluation jsonParser.parse(reader) as JsonElement
        } catch (e: Exception) {
        }
        throw Exception("Unreachable wiki-query")
    }

    /**
     * Update entity
     */
    override fun lock(update: Boolean) {
        locking = true
        if (!locked && updateNeeded) {
            updatedAt = DateTime.now()
        }
        super.lock(update)
    }

}
