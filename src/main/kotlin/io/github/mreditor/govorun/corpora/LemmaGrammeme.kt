package io.github.mreditor.govorun.corpora

import io.github.mreditor.govorun.entity.Entity
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.sql.ReferenceOption
import org.jetbrains.exposed.sql.Table as ExposedTable

/**
 * Word lemma grammeme object
 */
class LemmaGrammeme(id: EntityID<Int>) : Entity(id) {

    object Factory : Entity.Factory<LemmaGrammeme>(Table) {
        private val commonKeywordsGrammemes = listOf("NOUN", "ADJF", "VERB", "INFN")

        /**
         * Construct entity only if it contains grammeme of common part-of-speech
         */
        fun create(lemma: Lemma, grammeme: String) =
                if (grammeme in commonKeywordsGrammemes) new {
                    this.lemma = lemma
                    this.grammeme = grammeme
                } else {
                    null
                }

        /**
         * Find lemma
         */
        fun find(lemma: Lemma) = findInCache {
            Table.lemma eq lemma.id
        }.firstOrNull()
    }

    object Table : Entity.Table("lemma_grammeme") {
        val lemma = reference("lemma_id", Lemma.Table, ReferenceOption.CASCADE)
        val grammeme = varchar("value", 12)
    }

    var lemma by Lemma.Factory referencedOn Table.lemma
    var grammeme by Table.grammeme

}
