package io.github.mreditor.govorun.corpora

import io.github.mreditor.govorun.entity.Entity
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.sql.Table as ExposedTable

/**
 * Word lemma object
 */
class Lemma(id: EntityID<Int>) : Entity(id) {

    object Factory : Entity.Factory<Lemma>(Table) {
        fun create(value: String) = new {
            this.value = value
        }
    }

    object Table : Entity.Table("lemma") {
        val value = varchar("value", 255).index()
    }

    var value by Table.value

}
