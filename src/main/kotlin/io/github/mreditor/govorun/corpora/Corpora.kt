package io.github.mreditor.govorun.corpora

import io.github.mreditor.govorun.Database
import org.jetbrains.exposed.sql.deleteAll
import org.jetbrains.exposed.sql.transactions.transaction
import java.io.InputStream
import java.net.URL
import java.util.zip.ZipInputStream
import javax.xml.stream.XMLInputFactory

/**
 * Corpora source model
 */
class Corpora {

    companion object {
        private const val XML_NODE_LEMMA = "l"
        private const val XML_NODE_FORM = "f"
        private const val XML_NODE_GRAMMEME = "g"
    }

    /**
     * Update corpora with file by given URL
     */
    fun update(source: URL) {
        val connection = source.openConnection()
        val mime = connection.getHeaderField("Content-Type")
        when (mime) {
            "application/xml" -> {
                val xmlStream = connection.getInputStream()
                val length = connection.getHeaderField("Content-Length").toLong()
                update(xmlStream, length / 100)
            }
            "application/zip" -> {
                val zipStream = ZipInputStream(connection.getInputStream())
                val length = zipStream.nextEntry.size
                update(zipStream, length / 100)
            }
            else -> throw IllegalArgumentException("Unknown file type")
        }
    }

    /**
     * Update corpora with corpora from given stream
     */
    private fun update(stream: InputStream, statusEveryBytes: Long) {
        var position = 0
        update(object : InputStream() {
            override fun read(): Int {
                if (++position % statusEveryBytes == 0L) {
                    System.out.println("Updating corpora: ${position / statusEveryBytes}%")
                }
                return stream.read()
            }
        })
        System.out.println("Corpora updated!")
    }

    /**
     * Update corpora with given source
     */
    private fun update(source: InputStream) {
        val streamReader = XMLInputFactory.newInstance().createXMLStreamReader(source)
        lateinit var lemma: Lemma
        Database.dropTables(Lemma.Table, Form.Table, LemmaGrammeme.Table)
        Database.createTables()
        Database.aggregateToTransactions(streamReader::hasNext, 10 * Database.batchSize) {
            if (streamReader.isStartElement) when (streamReader.localName) {
                XML_NODE_LEMMA -> lemma = Lemma.Factory.create(
                        streamReader.getAttributeValue(null, "t")
                )
                XML_NODE_FORM -> Form.Factory.create(
                        lemma, streamReader.getAttributeValue(null, "t")
                )
                XML_NODE_GRAMMEME -> LemmaGrammeme.Factory.create(
                        lemma, streamReader.getAttributeValue(null, "v")
                )
            }
            streamReader.next()
        }
    }

}
