package io.github.mreditor.govorun.corpora

import io.github.mreditor.govorun.entity.Entity
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.sql.ReferenceOption
import org.jetbrains.exposed.sql.Table as ExposedTable

/**
 * Word form object
 */
class Form(id: EntityID<Int>) : Entity(id) {

    object Factory : Entity.Factory<Form>(Table) {
        fun create(lemma: Lemma, value: String) = new {
            this.lemma = lemma
            this.value = value
        }
    }

    object Table : Entity.Table("form") {
        val lemma = reference("lemma_id", Lemma.Table, ReferenceOption.CASCADE)
        val value = varchar("value", 255).index()
    }

    var lemma by Lemma.Factory referencedOn Table.lemma
    var value by Table.value

}
