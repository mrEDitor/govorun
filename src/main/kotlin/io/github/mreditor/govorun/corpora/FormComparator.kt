package io.github.mreditor.govorun.corpora

import io.github.mreditor.govorun.LRUCache
import io.github.mreditor.govorun.extensions.stops

/**
 * Form comparator
 */
open class FormComparator private constructor(val value: String) {

    /**
     * Instance fetcher
     */
    companion object {
        private val formToLemmas = LRUCache<String, FormComparator>()
        init {
            for (stop in stops) {
                formToLemmas.put(stop, object : FormComparator(stop) {
                    override fun equals(other: Any?): Boolean {
                        if (other == ".") {
                            return true
                        }
                        if (other is FormComparator) {
                            return other.value in stops
                        }
                        return false
                    }
                })
            }
        }

        /**
         * Get form by string
         */
        fun get(form: String) = formToLemmas.getOrPut(form) { FormComparator(form) }

        /**
         * Mass get forms by strings
         */
        fun map(forms: List<String>): List<FormComparator> {
            val formsToFetch = mutableListOf<String>()
            for (form in forms) {
                if (!formToLemmas.containsKey(form)) {
                    formsToFetch.add(form)
                }
            }
            warmFormsCache(formsToFetch)
            return forms.map { get(it) }
        }

        /**
         * Warm forms cache with mass select pre-fetch
         */
        protected fun warmFormsCache(values: List<String>) {
            Form.Factory.findInCache {
                Form.Table.value inList values
            }.toList()
        }
    }

    /**
     * Comparable forms list
     */
    val forms = Form.Factory.findInCache {
        Form.Table.value eq value
    }.toList()

    /**
     * Hash code evaluation
     */
    override fun hashCode() =
            this::class.hashCode() + forms.hashCode()

    /**
     * Equality evaluation
     */
    override fun equals(other: Any?): Boolean {
        if (this === other) return true

        if (other is String) {
            return _equals(other)
        }

        if (javaClass != other?.javaClass) return false
        other as FormComparator
        if (_equals(other.value)) {
            return true
        }
        for (theirForm in other.forms) {
            if (_equals(theirForm.id)) {
                return true
            }
        }
        if (other.forms.isEmpty() && _equals(other.wiki)) {
            return true
        }

        return false
    }

    /**
     * Compare object to all forms of this one
     */
    private fun _equals(form: Any): Boolean {
        if (value == form as? String) {
            return true
        }
        for (ourForm in forms) {
            if (ourForm == form) {
                return true
            }
        }
        if (forms.isEmpty() && wiki == form) {
            return true
        }
        return false
    }

    /**
     * Word distance measure
     */
    val distance by lazy { if (forms.isNotEmpty()) 0f else wiki.distance.toFloat() }

    /**
     * In corpora measure
     */
    val inCorporaMeasure by lazy {
        if (forms.isNotEmpty()) {
            1f - forms.sumBy {
                if (LemmaGrammeme.Factory.find(it.lemma) != null) 1 else 0
            } / forms.size.toFloat() / 2
        } else {
            .5f - distance / 2
        }
    }

    /**
     * Wiki data for term
     */
    val wiki by lazy { Wiki.Factory.find(value) }

    /**
     * Is phrase generic to [phrase]
     */
    fun isGenericTo(phrase: List<FormComparator>): Boolean {
        for (forms in phrase) {
            for (form in forms.forms) {
                if (form.value == value) {
                    return true
                }
            }
        }
        return false
    }

    /**
     * Is phrase contain naming terms
     */
    val containsNamingTerms: Boolean
        get() = forms.isEmpty() || forms.any {
            "NOUN" == LemmaGrammeme.Factory.find(it.lemma)?.grammeme
        }

}
