package io.github.mreditor.govorun

import io.github.mreditor.govorun.corpora.Form
import io.github.mreditor.govorun.corpora.FormComparator
import io.github.mreditor.govorun.document.*
import io.github.mreditor.govorun.extensions.toComparableForms
import io.github.mreditor.govorun.perceptron.Configuration
import io.github.mreditor.govorun.perceptron.Perceptron
import io.github.mreditor.govorun.perceptron.PerceptronLayer
import org.jetbrains.exposed.sql.transactions.transaction
import java.util.*
import kotlin.math.max
import kotlin.math.min

/**
 * Main extractor mechanic class
 */
class Govorun(val perceptron: Perceptron = createPerceptron()) {

    /**
     * Create perceptron
     */
    companion object {
        private fun createPerceptron(seed: Long? = null): Perceptron {
            val random = Random(seed ?: CLI.active().getString(setting = "training.seed").toLong())
            return Perceptron(
                    PerceptronLayer(8, random),
                    PerceptronLayer(2, random)
            )
        }
    }

    /**
     * Train govorun
     */
    fun train(seed: Long, totalExamples: Int, epochs: Int) {
        val documentSet = DocumentSet(totalExamples, seed)
        val trainingSetSize = documentSet.exampleSet.size * 3 / 4
        val bestConfiguration = Configuration.Factory.getOrSave(perceptron)
        for (i in 1..epochs) {
            if (i % (epochs / 100) == 0) {
                System.out.println("${i}th epoch of ${epochs}")
            }
            perceptron.clearError()
            for (example in documentSet.exampleSet.subList(0, trainingSetSize)) {
                perceptron.adapt(example, 0.7f)
            }
            perceptron.error /= trainingSetSize
            if (perceptron.error < bestConfiguration.error.toFloat()) {
                bestConfiguration.save(perceptron)
            }
        }
        validate(documentSet)
    }

    /**
     * Validate govorun
     */
    fun validate(seed: Long, totalExamples: Int) {
        validate(DocumentSet(totalExamples, seed))
    }

    /**
     * Validate govorun
     */
    fun validate(documentSet: DocumentSet) {
        var truePositives = 0f
        var falseNegatives = 0f
        var falsePositives = 0f
        val perceptron = Configuration.Factory.get().load()
        for (i in documentSet.exampleSet.indices) {
            val example = documentSet.exampleSet[i]
            perceptron.solve(example)
            System.out.println("$i) " + example.phrase.map { it.value }.joinToString(" ") + ":")
            for (j in example.output) System.out.print("$j\t")
            for (j in perceptron.output.output) System.out.print("$j\t")
            val solution = example.output.withIndex().maxBy { it.value }?.index
            val answer = perceptron.output.output.withIndex().maxBy { it.value }?.index
            if (solution == answer) {
                if (answer == DocumentPhrase.OUTPUT_TRUE) {
                    ++truePositives
                } else {
                    ++falseNegatives
                }
            } else if (answer == DocumentPhrase.OUTPUT_TRUE) {
                ++falsePositives
            }
            System.out.println()
            System.out.println()
        }
        System.out.println("Precision is ${truePositives / (truePositives + falsePositives)}")
        System.out.println("Recall is ${truePositives / (truePositives + falseNegatives)}")
    }

    /**
     * Extract keywords from text
     */
    fun extractKeywords(text: String, count: Int = 20): List<String> {
        val maxLength = CLI.active().getString(setting = "extract.phrase.length").toInt()
        val keywords = LinkedHashMap<List<FormComparator>, DocumentPhrase>()
        val forms = text.toComparableForms().toList()
        val documentsCount = Document.Factory.all().count()
        val stop = FormComparator.get(".")
        (0 until forms.size).toList().parallelStream().forEach { offset ->
            transaction (Database.connection) {
                for (length in 1..min(maxLength, forms.size - offset)) {
                    try {
                        val candidate = forms.subList(offset, offset + length)
                        if (!candidate.any { it.containsNamingTerms }) {
                            continue
                        } else if (keywords.contains(candidate)) {
                            if (candidate.any { it.isGenericTo(keywords.getValue(candidate).phrase) }) {
                                keywords[candidate] = DocumentPhrase(candidate, forms, documentsCount)
                            }
                        } else if (candidate.indexOf(stop) < 0) {
                            val phrase = DocumentPhrase(candidate, forms, documentsCount)
                            perceptron.solve(phrase)
                            for (i in 0 until DocumentPhrase.OUTPUT_SIZE) {
                                phrase.output[i] = perceptron.output.external[i]
                            }
                            if (phrase.output[DocumentPhrase.OUTPUT_FALSE]
                                    < phrase.output[DocumentPhrase.OUTPUT_TRUE]) {
                                keywords[candidate] = phrase
                            }
                        }
                    } catch (e: DocumentTag.InextractableTagException) {
                    }
                }
            }
        }
        val keywordsList = keywords.values
        val result = keywordsList
                .sortedBy { -it.output[DocumentPhrase.OUTPUT_TRUE] }
                .subList(0, min(keywordsList.size, count * max(4, count / 2)))
                /*
                .sortedBy { it.joined }
                .fold(arrayListOf<DocumentPhrase>()) { accum, phrase ->
                    if (accum.isEmpty() || accum.last() distanceTo phrase > 0.6f) {
                        accum.add(phrase)
                    }
                    return@fold accum
                }
                */
                .sortedBy { -it.output[DocumentPhrase.OUTPUT_TRUE] }
                .map { it.joined }
                .distinct()
        return result.subList(0, min(result.size, count))
    }

    fun warmCorporaCache() = transaction {
        val limit = CLI.active().getString(setting = "extract.vocabulary.preload").toInt()
        for (term in Vocabulary.Factory.mostCountableTerms(limit)) {
            Form.Factory.findInCache {
                Form.Table.value eq term
            }
        }
    }

}
