package io.github.mreditor.govorun

import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.google.gson.JsonPrimitive
import io.github.mreditor.govorun.CLI.Companion.launch
import io.github.mreditor.govorun.corpora.Corpora
import io.github.mreditor.govorun.corpora.Wiki
import io.github.mreditor.govorun.document.DocumentSet
import io.github.mreditor.govorun.extensions.toRange
import io.github.mreditor.govorun.perceptron.Configuration
import io.javalin.Javalin
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IdTable
import org.jetbrains.exposed.sql.transactions.transaction
import java.net.URL

fun main(args: Array<String>) = launch(args) {
    val MIME_JSON = "application/json"

    registerCommand("show-config", null, "Show current configuration") {
        System.out.println(dumpConfiguration().joinToString("\n"))
    }
    registerCommand("corpora", "[URL]", "Download language corpora given by specified URL") {
        Database.connect()
        Corpora().update(URL(getString("corpora", "corpora.url")))
    }
    registerCommand("clear", null, "Clear document base") {
        Database.connect()
        DocumentSet().clear()
    }
    registerCommand("habr", "X-Y", "Download [X-Y] posts from habr.com to document base") {
        Database.connect()
        DocumentSet().add("habr", getString("habr").toRange())
    }
    registerCommand("train", "[SEED]", "Train on current document base with given SEED and --size") {
        Database.connect()
        Govorun().train(
                getString("train", "training.seed").toLong(),
                getString("size", "training.phrases").toInt(),
                getString("epochs", "training.epochs").toInt()
        )
    }
    registerCommand("validate", "[SEED]", "Validate on current document base with given SEED and --size") {
        Database.connect()
        Govorun().validate(
                getString("train", "training.seed").toLong(),
                getString("size", "training.phrases").toInt()
        )
    }
    registerCommand("start", "[PORT]", "Listen for incoming keyphrase extraction requests") {
        val port = getString("start", "web.port").toInt()
        System.out.println("Starting Govorun on port ${port}...")
        Database.connect()
        val configuration = Configuration.Factory.get()
        val govorun = Govorun(configuration.load())
        govorun.warmCorporaCache()
        System.out.println("Govorun ready")
        val app = Javalin.start(port)
        app.get("/") {
            it.result("Govorun keyphrase extractor REST API").contentType(MIME_JSON)
        }
        app.post("/extract_keywords") {
            try {
                val startedAt = System.currentTimeMillis()
                val request = JsonParser().parse(it.body())
                val text = request.asJsonObject["text"].asJsonPrimitive.asString
                val response = JsonObject()
                val keywords = JsonArray().apply {
                    transaction {
                        govorun.extractKeywords(text).map { add(it) }
                    }
                }
                response.add("status", JsonPrimitive("success"))
                response.add("keywords", keywords)
                System.out.println("OK in " + (System.currentTimeMillis() - startedAt))
                it.result(response.toString()).contentType(MIME_JSON)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        while (true) try {
            Thread.sleep(1000)
        } catch (e: InterruptedException) {
            app.stop()
            return@registerCommand
        }
    }
    if (!anyCommandSuccess || anyCommandFailed) {
        System.out.println("""
            Govorun: keyphrase extractor
            Is distinguished by intelligence and ingenuity.

            Project page: https://gitlab.com/mrEDitor/govorun
            Contact developer: mrEDitor@mail.ru
        """.trimIndent())
        showHelp()
    }
}
