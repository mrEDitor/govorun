package io.github.mreditor.govorun.document

import io.github.mreditor.govorun.corpora.FormComparator
import io.github.mreditor.govorun.entity.Entity
import io.github.mreditor.govorun.extensions.toComparableForms
import io.github.mreditor.govorun.extensions.toTerms
import org.jetbrains.exposed.dao.EntityID
import java.math.BigDecimal

/**
 * Document model
 */
class Document(id: EntityID<Int>) : Entity(id) {

    object Factory : Entity.Factory<Document>(Table) {
        fun create(text: String, tags: List<String>, source: String? = null) = new {
            this.text = text
            this.source = source
            this.inVocabulary = false
        }.apply {
            for (tag in tags) try {
                DocumentTag.Factory.create(this, tag)
            } catch (e: DocumentTag.InextractableTagException) {
            }
            lock()
        }
    }

    object Table : Entity.Table("document") {
        val text = text("text")
        val importSource = varchar("source", 64).nullable()
        val tagsFrequency = decimal("tags_frequency").default(BigDecimal.ZERO).index()
    }

    var text by lockable(Table.text)
    var source by lockable(Table.importSource)
    val tags by DocumentTag.Factory referrersOn DocumentTag.Table.document
    val tagsFrequency by Table.tagsFrequency evaluates {
        if (forms.isNotEmpty()) {
            decimal(tagsInText) / decimal(forms.size)
        } else {
            BigDecimal.ZERO
        }
    }

    /**
     * Comparable forms in text
     */
    val forms by evaluation {
        val forms = text.toComparableForms().toList()
        formVocabulary(forms)
        forms
    }

    /**
     * Count of tags in text
     */
    val tagsInText by evaluation {
        tags.sumBy {
            if (it.positionInTextAbsolute != null) {
                it.value.toTerms().count()
            } else {
                0
            }
        }
    }

    /**
     * Is saved to vocabulary
     */
    private var inVocabulary = true

    /**
     * Save to form vocabulary
     */
    private fun formVocabulary(forms: List<FormComparator>) {
        if (inVocabulary) {
            return
        }
        for (form in forms) {
            form.forms.forEach { Vocabulary.Factory.inc(it.value, id.value) }
            Vocabulary.Factory.inc(form.value, id.value)
        }
        inVocabulary = true
    }

    /**
     * Do not save document without tags
     */
    override fun lock(update: Boolean) {
        locking = true
        if (tags.empty()) {
            /* TODO remove */ delete()
            throw DocumentTag.InextractableTagException()
        }
        super.lock(update)
    }
}
