package io.github.mreditor.govorun.document.source

import io.github.mreditor.govorun.CLI
import io.github.mreditor.govorun.document.Document
import io.github.mreditor.govorun.extensions.toTerms
import org.jsoup.HttpStatusException
import org.jsoup.Jsoup

/**
 * Habr.com document source
 */
class Habr : Source {

    /**
     * Import document by id
     */
    override fun import(id: Int): Document {
        val urlTemplate = CLI.active().getString(null, "habr.post.url")
        val textSelector = CLI.active().getString(null, "habr.selector.text")
        val codeSelector = CLI.active().getString(null, "habr.selector.code")
        val tagSelector = CLI.active().getString(null, "habr.selector.tag")

        val url = urlTemplate.format(id)
        System.out.print("Fetching ${url} ... ")
        val html = Jsoup.connect(url).get()
        val tags = html.select(tagSelector).map { it.text().toTerms().joinToString(" ") }.toList()
        val document = html.select(textSelector)
        document.select(codeSelector).remove()

        return Document.Factory.create(document.text(), tags, "habr#${id}")
    }

}
