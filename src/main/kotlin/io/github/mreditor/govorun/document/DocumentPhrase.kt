package io.github.mreditor.govorun.document

import io.github.mreditor.govorun.corpora.FormComparator
import io.github.mreditor.govorun.extensions.*
import io.github.mreditor.govorun.perceptron.SourceLayer
import kotlin.math.max
import kotlin.math.min

/**
 * Document phrase model
 */
class DocumentPhrase : SourceLayer {

    class FalseNegativeException : Exception()

    companion object {
        private const val PHRASE_FREQUENCY = 0
        private const val TERM_FREQUENCY_MIN = 1
        private const val TERM_FREQUENCY_AVG = 2
        private const val TERM_FREQUENCY_MAX = 3
        private const val DOCUMENT_FREQUENCY_MIN = 4
        private const val DOCUMENT_FREQUENCY_AVG = 5
        private const val DOCUMENT_FREQUENCY_MAX = 6
        private const val IS_IN_CORPORA_MIN = 7
        private const val IS_IN_CORPORA_AVG = 8
        private const val IS_IN_CORPORA_MAX = 9
        private const val POSITION_IN_TEXT = 10
        private const val POSITION_IN_SENTENCE = 11
        const val INPUT_SIZE = 12
        const val OUTPUT_SIZE = 2
        const val OUTPUT_FALSE = 0
        const val OUTPUT_TRUE = 1
    }

    /**
     * Phrase to evaluate
     */
    val phrase: List<FormComparator>

    /**
     * Joined phrase
     */
    val joined: String

    /**
     * Input metrics
     */
    val input = FloatArray(INPUT_SIZE)

    /**
     * Is phrase keyword
     */
    val output = FloatArray(OUTPUT_SIZE)

    /**
     * Get external layer data
     */
    override val external: FloatArray
        get() = input

    /**
     * Source document id
     */
    val document: Int

    /**
     * Not a keyphrase
     */
    constructor(document: Document, offset: Int, initLength: Int, documentSetSize: Int) {
        this.document = document.id.value
        val length = min(initLength, document.forms.size - offset)
        phrase = document.forms.subList(offset, offset + length)
        input[PHRASE_FREQUENCY] = phraseFrequency(document.forms, phrase)
        input[TERM_FREQUENCY_MIN] = 1.0f
        input[TERM_FREQUENCY_AVG] = 0.0f
        input[TERM_FREQUENCY_MAX] = 0.0f
        input[DOCUMENT_FREQUENCY_MIN] = 1.0f
        input[DOCUMENT_FREQUENCY_AVG] = 0.0f
        input[DOCUMENT_FREQUENCY_MAX] = 0.0f
        input[IS_IN_CORPORA_MIN] = 1.0f
        input[IS_IN_CORPORA_AVG] = 0.0f
        input[IS_IN_CORPORA_MAX] = 0.0f
        for (i in 0 until length) {
            val termFrequency = termFrequency(document.forms, document.forms[offset + i])
            val documentFrequency = max(1.0f, documentFrequency(document.forms[offset + i], documentSetSize))
            input[TERM_FREQUENCY_MIN] = min(input[TERM_FREQUENCY_MIN], termFrequency)
            input[TERM_FREQUENCY_AVG] += termFrequency
            input[TERM_FREQUENCY_MAX] = max(input[TERM_FREQUENCY_MIN], termFrequency)
            input[DOCUMENT_FREQUENCY_MIN] = min(input[DOCUMENT_FREQUENCY_MIN], documentFrequency)
            input[DOCUMENT_FREQUENCY_AVG] += documentFrequency
            input[DOCUMENT_FREQUENCY_MAX] = max(input[DOCUMENT_FREQUENCY_MIN], documentFrequency)
            input[IS_IN_CORPORA_MIN] = min(input[IS_IN_CORPORA_MIN], document.forms[offset + i].inCorporaMeasure)
            input[IS_IN_CORPORA_AVG] += document.forms[offset + i].inCorporaMeasure
            input[IS_IN_CORPORA_MAX] = max(input[IS_IN_CORPORA_MAX], document.forms[offset + i].inCorporaMeasure)
        }
        input[TERM_FREQUENCY_AVG] /= length.toFloat()
        input[DOCUMENT_FREQUENCY_AVG] /= length.toFloat()
        input[IS_IN_CORPORA_AVG] /= length.toFloat()
        input[POSITION_IN_TEXT] = offset.toFloat() / document.forms.size
        input[POSITION_IN_SENTENCE] = document.forms.positionInSentence(offset) / document.forms.size
        output[OUTPUT_FALSE] = 1f
        joined = phrase.joinToString(" ") { it.value }
    }

    /**
     * A keyphrase
     */
    constructor(tag: DocumentTag, documentSetSize: Int) {
        val document = tag.document
        this.document = document.id.value
        phrase = tag.forms
        input[PHRASE_FREQUENCY] = tag.countInText.toFloat()
        input[TERM_FREQUENCY_MIN] = 1.0f
        input[TERM_FREQUENCY_AVG] = 0.0f
        input[TERM_FREQUENCY_MAX] = 0.0f
        input[DOCUMENT_FREQUENCY_MIN] = 1.0f
        input[DOCUMENT_FREQUENCY_AVG] = 0.0f
        input[DOCUMENT_FREQUENCY_MAX] = 0.0f
        input[IS_IN_CORPORA_MIN] = 1.0f
        input[IS_IN_CORPORA_AVG] = 0.0f
        input[IS_IN_CORPORA_MAX] = 0.0f
        for (i in 0 until tag.forms.size) {
            val termFrequency = termFrequency(document.forms, tag.forms[i])
            val documentFrequency = max(1.0f, documentFrequency(tag.forms[i], documentSetSize))
            input[TERM_FREQUENCY_MIN] = min(input[TERM_FREQUENCY_MIN], termFrequency)
            input[TERM_FREQUENCY_AVG] += termFrequency
            input[TERM_FREQUENCY_MAX] = max(input[TERM_FREQUENCY_MIN], termFrequency)
            input[DOCUMENT_FREQUENCY_MIN] = min(input[DOCUMENT_FREQUENCY_MIN], documentFrequency)
            input[DOCUMENT_FREQUENCY_AVG] += documentFrequency
            input[DOCUMENT_FREQUENCY_MAX] = max(input[DOCUMENT_FREQUENCY_MIN], documentFrequency)
            input[IS_IN_CORPORA_MIN] = min(input[IS_IN_CORPORA_MIN], tag.forms[i].inCorporaMeasure)
            input[IS_IN_CORPORA_AVG] += tag.forms[i].inCorporaMeasure
            input[IS_IN_CORPORA_MAX] = max(input[IS_IN_CORPORA_MAX], tag.forms[i].inCorporaMeasure)
        }
        input[TERM_FREQUENCY_AVG] /= tag.forms.size.toFloat()
        input[DOCUMENT_FREQUENCY_AVG] /= tag.forms.size.toFloat()
        input[IS_IN_CORPORA_AVG] /= tag.forms.size.toFloat()
        input[POSITION_IN_TEXT] = tag.positionInText.toFloat()
        input[POSITION_IN_SENTENCE] = tag.positionInSentence.toFloat()
        output[OUTPUT_TRUE] = 1f
        joined = phrase.joinToString(" ") { it.value }
    }

    /**
     * Create phrase from text
     */
    constructor(phrase: List<FormComparator>, text: List<FormComparator>, documentSetSize: Int) {
        this.document = 0
        this.phrase = phrase
        input[PHRASE_FREQUENCY] = phraseFrequency(text, phrase)
        input[TERM_FREQUENCY_MIN] = 1.0f
        input[TERM_FREQUENCY_AVG] = 0.0f
        input[TERM_FREQUENCY_MAX] = 0.0f
        input[DOCUMENT_FREQUENCY_MIN] = 1.0f
        input[DOCUMENT_FREQUENCY_AVG] = 0.0f
        input[DOCUMENT_FREQUENCY_MAX] = 0.0f
        input[IS_IN_CORPORA_MIN] = 1.0f
        input[IS_IN_CORPORA_AVG] = 0.0f
        input[IS_IN_CORPORA_MAX] = 0.0f
        for (i in 0 until phrase.size) {
            val termFrequency = termFrequency(text, phrase[i])
            val documentFrequency = max(1.0f, documentFrequency(phrase[i], documentSetSize))
            input[TERM_FREQUENCY_MIN] = min(input[TERM_FREQUENCY_MIN], termFrequency)
            input[TERM_FREQUENCY_AVG] += termFrequency
            input[TERM_FREQUENCY_MAX] = max(input[TERM_FREQUENCY_MIN], termFrequency)
            input[DOCUMENT_FREQUENCY_MIN] = min(input[DOCUMENT_FREQUENCY_MIN], documentFrequency)
            input[DOCUMENT_FREQUENCY_AVG] += documentFrequency
            input[DOCUMENT_FREQUENCY_MAX] = max(input[DOCUMENT_FREQUENCY_MIN], documentFrequency)
            input[IS_IN_CORPORA_MIN] = min(input[IS_IN_CORPORA_MIN], phrase[i].inCorporaMeasure)
            input[IS_IN_CORPORA_AVG] += phrase[i].inCorporaMeasure
            input[IS_IN_CORPORA_MAX] = max(input[IS_IN_CORPORA_MAX], phrase[i].inCorporaMeasure)
        }
        input[TERM_FREQUENCY_AVG] /= phrase.size.toFloat()
        input[DOCUMENT_FREQUENCY_AVG] /= phrase.size.toFloat()
        input[IS_IN_CORPORA_AVG] /= phrase.size.toFloat()
        val position = text.indexOfSubList(phrase)
        if (position < 0) {
            throw DocumentTag.InextractableTagException()
        }
        val previousStop = text.lastIndexOf(*stops, toIndex = position)
        val nextStop = text.indexOf(*stops, fromIndex = position)
        input[POSITION_IN_TEXT] = position.toFloat() / text.size
        input[POSITION_IN_SENTENCE] = (position - previousStop).toFloat() /
                ((if (nextStop < 0) text.size else nextStop) - previousStop - 1)
        joined = phrase.joinToString(" ") { it.value }
    }

    /**
     * Phrase frequency evaluation
     */
    private fun phraseFrequency(forms: List<FormComparator>, phrase: List<FormComparator>) =
            forms.countOf(phrase) / forms.size.toFloat()

    /**
     * Term frequency evaluation
     */
    private fun termFrequency(forms: List<FormComparator>, phrase: FormComparator) =
            forms.countOf(phrase) / forms.size.toFloat()

    /**
     * Document frequency evaluation
     */
    private fun documentFrequency(form: FormComparator, documentSetSize: Int) =
            Vocabulary.Factory.find(form.value).firstOrNull()
                    ?.count?.toFloat()?.div(documentSetSize)
                    ?: 1f

    /**
     * Distance between
     */
    infix fun distanceTo(other: DocumentPhrase) =
            other.phrase.joinToString(" ") { it.value }.distance(joined)

    /**
     * Comparator
     */
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as DocumentPhrase
        if (phrase != other.phrase) return false
        if (document != other.document) return false

        return true
    }

    /**
     * Hash code of object
     */
    override fun hashCode(): Int {
        var result = phrase.hashCode()
        result = 31 * result + document
        return result
    }


}
