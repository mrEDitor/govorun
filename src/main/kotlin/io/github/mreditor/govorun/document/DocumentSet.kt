package io.github.mreditor.govorun.document

import io.github.mreditor.govorun.CLI
import io.github.mreditor.govorun.Database
import io.github.mreditor.govorun.document.source.Source
import org.jetbrains.exposed.sql.transactions.transaction
import org.jsoup.HttpStatusException
import java.net.SocketException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import java.util.*
import kotlin.collections.ArrayList

/**
 * Document set model
 */
class DocumentSet {

    /**
     * Examples
     */
    val exampleSet: ArrayList<DocumentPhrase>

    /**
     * Empty constructor
     */
    constructor() {
        exampleSet = arrayListOf()
    }

    /**
     * Iterable constructor
     */
    constructor(totalExamples: Int = 0, seed: Long = 0) {
        exampleSet = ArrayList(totalExamples)
        transaction {
            val random = Random(seed)
            val documentTags = DocumentTag.Factory.all()
            val documents = Document.Factory.all()
            val tagSetSize = documentTags.count()
            /* TODO val */
            var documentSetSize = documents.count()
            val phraseLength = CLI.active().getString(setting = "training.phrase.length").toInt()
            while (exampleSet.size < totalExamples / 2) {
                exampleSet.add(DocumentPhrase(
                        DocumentTag.Factory.getNth(documentTags, random.nextInt(tagSetSize)),
                        documentSetSize
                ))
                System.out.println("${100 * exampleSet.size / totalExamples}% phrases loaded")
            }
            /* TODO val */
            lateinit var document: Document
            while (exampleSet.size < totalExamples) try {
                document = Document.Factory.getNth(documents, random.nextInt(documentSetSize))
                exampleSet.add(DocumentPhrase(
                        document,
                        random.nextInt(document.forms.size),
                        1 + random.nextInt(phraseLength - 1),
                        documentSetSize
                ))
                System.out.println("${100 * exampleSet.size / totalExamples}% phrases loaded")
            } catch (e: DocumentPhrase.FalseNegativeException) {
            } catch (e: DocumentTag.InextractableTagException) {
                /* TODO remove */ --documentSetSize
            }
            exampleSet.shuffle(random)
        }
    }

    /**
     * Importers package
     */
    private val importersPackage = Source::class.java.`package`

    /**
     * Add document by its importer type and id
     */
    fun add(importer: String, ids: IntRange) {
        val importerClassName = importersPackage.name + "." + importer.capitalize()
        val importerClass = Class.forName(importerClassName).newInstance() as? Source
                ?: throw IllegalArgumentException("${importer} is not a valid importer type")
        for (id in ids) {
            while (true) {
                try {
                    transaction {
                        try {
                            val document = importerClass.import(id)
                            System.out.println(
                                    "Document #${id} with ${document.tagsFrequency} of tags has been imported."
                            )
                        } catch (e: DocumentTag.InextractableTagException) {
                            System.out.println(
                                    "Document #${id} was rejected due to zero extractable tags."
                            )
                        }
                    }
                    break
                } catch (e: UnknownHostException) {
                    System.out.println("Document #${id} reloading due to DNS error.")
                } catch (e: SocketTimeoutException) {
                    System.out.println("Document #${id} reloading due to socket timeout.")
                } catch (e: SocketException) {
                    System.out.println("Document #${id} reloading due to socket error.")
                } catch (e: HttpStatusException) {
                    System.out.println("Document #${id} has been skipped due to HTTP ${e.statusCode}.")
                    break
                }
            }
        }
    }

    /**
     * Clear document set
     */
    fun clear() {
        Database.dropTables(Document.Table, DocumentTag.Table, Vocabulary.Table)
    }

}
