package io.github.mreditor.govorun.document

import io.github.mreditor.govorun.entity.Entity
import io.github.mreditor.govorun.extensions.*
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.sql.ReferenceOption
import java.math.BigDecimal

/**
 * Document tag model
 */
class DocumentTag(id: EntityID<Int>) : Entity(id) {

    class InextractableTagException : Exception("Tag can not be extracted")

    object Factory : Entity.Factory<DocumentTag>(Table) {
        fun create(document: Document, value: String) = new {
            this.document = document
            this.value = value
            lock()
        }
    }

    object Table : Entity.Table("document_tag") {
        val document = reference("document_id", Document.Table, ReferenceOption.CASCADE)
        val value = varchar("value", 255)
        val countInTextAbsolute = integer("count_in_text_abs").index()
        val countInText = decimal("count_in_text").index()
        val positionInSentenceAbsolute = integer("position_in_sentence_abs").index()
        val positionInSentence = decimal("position_in_sentence").index()
        val positionInTextAbsolute = integer("position_in_text_abs").index()
        val positionInText = decimal("position_in_text").index()
    }

    var document by lockable(Document.Factory referencedOn Table.document)
    var value by lockable(Table.value)
    val positionInTextAbsolute by Table.positionInTextAbsolute evaluates {
        document.forms.indexOfSubList(forms)
    }
    val positionInText by Table.positionInText evaluates {
        if (document.forms.isNotEmpty()) {
            decimal(positionInTextAbsolute).div(decimal(document.forms.size))
        } else {
            -BigDecimal.ONE
        }
    }
    val positionInSentenceAbsolute by Table.positionInSentenceAbsolute evaluates {
        val previousStop = document.forms.lastIndexOf(*stops, toIndex = positionInTextAbsolute)
        return@evaluates positionInTextAbsolute - previousStop - 1
    }
    val positionInSentence by Table.positionInSentence evaluates {
        document.forms.positionInSentence(positionInTextAbsolute).toBigDecimal()
    }
    val countInTextAbsolute by Table.countInTextAbsolute evaluates {
        document.forms.countOf(forms)
    }
    val countInText by Table.countInText evaluates {
        if (document.forms.isNotEmpty()) {
            decimal(countInTextAbsolute) / decimal(document.forms.size)
        } else {
            BigDecimal.ZERO
        }
    }

    /**
     * Comparable forms in tag
     */
    val forms
        get() = value.toComparableForms().toList()

    /**
     * Do not save tags, terms of which was not found in text
     */
    override fun lock(update: Boolean) {
        locking = true
        if (countInTextAbsolute <= 0 || positionInTextAbsolute < 0) {
            throw InextractableTagException()
        }
        super.lock(update)
    }
}
