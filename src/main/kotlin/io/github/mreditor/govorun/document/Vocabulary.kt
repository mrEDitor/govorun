package io.github.mreditor.govorun.document

import io.github.mreditor.govorun.entity.Entity
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.sql.SortOrder
import org.jetbrains.exposed.sql.selectAll

/**
 * Vocabulary model
 */
class Vocabulary(id: EntityID<Int>) : Entity(id) {

    object Factory : Entity.Factory<Vocabulary>(Table) {

        /**
         * Fetch vocabulary entry by term
         */
        fun create(term: String) = new {
            this.term = term
        }.apply { lock() }

        /**
         * Find vocabulary entry by term or fetch new
         */
        fun find(term: String): List<Vocabulary> {
            val vocabulary = findInCache {
                Table.term eq term
            }
            if (vocabulary.any()) {
                return vocabulary.map { it.apply { lock() } }
            } else {
                return listOf(create(term))
            }
        }

        /**
         * Increment vocabulary entry
         */
        fun inc(term: String, documentId: Int) = find(term).forEach { it.inc(documentId) }

        /**
         * Top [limit] countable words
         */
        fun mostCountableTerms(limit: Int): List<String> =
                Table.selectAll().orderBy(Pair(Table.count, SortOrder.DESC)).limit(limit)
                        .map { it[Table.term] }

    }

    object Table : Entity.Table("vocabulary") {
        val term = varchar("term", 255).uniqueIndex()
        val count = integer("count").default(0)
    }

    var term by lockable(Table.term)
    var count by Table.count

    /**
     * Last document id
     */
    var lastDocumentId = 0

    /**
     * Increment counter
     */
    private fun inc(documentId: Int) {
        if (lastDocumentId != documentId) {
            lastDocumentId = documentId
            ++count
        }
    }

}
