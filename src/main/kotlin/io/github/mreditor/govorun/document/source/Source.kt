package io.github.mreditor.govorun.document.source

import io.github.mreditor.govorun.CLI
import io.github.mreditor.govorun.document.Document
import io.github.mreditor.govorun.extensions.toTerms
import org.jsoup.Jsoup

/**
 * Abstract document source
 */
interface Source {

    /**
     * Import document by id
     */
    fun import(id: Int): Document

}
